import React, { Component } from 'react';
import Button from './Button';

class Buttons extends Component{
    render(){
        return(
        <>
         <div className="d-flex justify-content-around "
        style={{
          margin: '0 200px'
        }}>
          
        <Button 
            color={'btn btn-info'}
            text={"+1"} 
            handleOnClick={this.props.handleAdd}
            />
        <Button 
            color = {'btn btn-warning'}
            text={"-1"}
            handleOnClick={this.props.handleMinus}
            />
        <Button 
        color = {'btn btn-success'}
        text={"Reset"}
        handleOnClick={this.props.handleReset}
        />
        <Button 
        color = {'btn btn-secondary'}
        text={"x2"}
        handleOnClick={this.props.handleMultiply}
        />
      </div>
        </>
            )
    }
}
export default Buttons;