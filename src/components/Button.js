import React, { Component } from 'react';

class Button extends Component{
    render(){
        return(
            <>
            <button 
            className={this.props.color}
            onClick={this.props.handleOnClick}>
                {this.props.text}</button>
            </>
        )
    }
}

export default Button;